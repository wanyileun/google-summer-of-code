<!--
.. title: Guidelines: ranking proposals
.. slug: ranking-proposals
.. author: Christian Frisson
.. date: 2023-02-27 16:33:11 UTC-04:00
.. tags: guidelines
.. type: text
-->

These guidelines describe the process of ranking proposals from potential Contributors, empirically defined during our first ranking process in 2022.

## Process

We assign two reviewers per proposal:

* 1 Internal reviewer: one of the main authors of the project idea related to proposal
* 1 External reviewer: not a co-mentor of the project idea related to proposal

### Scores

Each reviewer evaluates each proposal by giving scores:

| Criterion | Range | Weight |
| --- | --- | --- |
| Has the Contributor opened at least one issue? | 0 (No) - 1 (Yes) | 4 |
| Has the Contributor opened at least one merge request? | 0 (No) - 1 (Yes) | 4 |
| Has the Contributor been efficient in submitting their proposal? | 0 (Not efficient: required lengthy discussions) - 1 - 3 (Very efficient: only minor suggestions needed) | 0(*) |
| How would you rate the efficiency of the Contributor in writing textual content (in issues, proposals, towards doc and potential publications)? | 0-6 | 2 |
| Have you had mutual discussions with the Contributor? If so, how would you rate the communication skills of the Contributor? | 0 (Yes, had discussions; and Great, appreciates advice) - 1 (No, no discussions) - 2 (Yes, had discussions; and Poor, rejects advice) | 3 |
| How would you rate the Benefits to the community (SAT): art, culture, inclusivity (accessibility)...? | 0 (Low) - 1 - 2 (High) | 2 |
| How would you rate the Feasibility of the proposed timeline? | 0 (Low) - 1 - 2 (High) | 3 |
| How would you rate the Contributor's background fitness to art AND/OR science/tech ? | 0 (None, neither art nor science/tech) - 1 - 2 (All, both art and science/tech) | 3 |
| How would you rate the Contributor's fitness to optional/required skills? | 0 (Low) - 1 - 2 (High) | 3 |
| Have you already collaborated with the Contributor before GSoC? If so, would you recommend collaborating with the Contributor? | 0 (Yes, collaborated before; and No, does not recommend) - 1 (No, not collaborated before) - 2 (Yes, collaborated before; and Yes, recommends) | 2 |

(*) considered but dropped in 2022

### Comments

Each review can contain free-form comments:

* Would you have negative comments to highlight?
* Would you have any other comments to share?

We then obtain a ranked list and ask Google for n slots, with n equal to or less than: half of the amount of available mentors and the rank of the last proposal we want to accept.
