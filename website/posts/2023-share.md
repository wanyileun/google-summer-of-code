<!--
.. title: Call for GSoC proposals at SAT
.. slug: 2023-share
.. author: Christian Frisson
.. date: 2023-02-24 17:04:11 UTC-05:00
.. tags: 
.. type: text
-->
# Call for GSoC proposals at SAT

Please help us connect with potential student contributors who wish to apply for GSoC proposals at SAT!

Please contribute to our known list of communication channels for sharing: [https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/issues/75](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/issues/75)

## Title

Please use the following line as title for emails or other communications media featuring titles, otherwise please consider starting your message with this line:

> GSoC 2023 - Call for Google Summer of Code student contributors' proposals at SAT

## Message

Please use the following content for the body of your message:

### About Google Summer of Code

[Google Summer of Code](https://g.co/gsoc) ([g.co/gsoc](https://g.co/gsoc)) is Google's mentorship program for bringing new contributors into open source communities. It's happening again for the 19th year in 2023! Over 18,000 developers from 112 countries have participated.

Google Summer of Code is a unique program where new contributors to open source, ages 18 and over, are paired with a mentor to introduce them to the open source community and provide guidance while they work on a real world open source project over the summer. Projects cover a wide range of fields including: Cloud, Operating Systems, Graphics, Medicine, Programming Languages, Robotics, Science, Security and many more. GSoC Contributors do earn a stipend to work on their medium (~175 hour) or large (~350 hour) projects. This is not an internship but provides an invaluable experience and allows you to be part of an amazing community!

GSoC is a highly competitive program, so don't wait to the last minute to prepare! GSoC Contributors should reach out to the organizations that interest them once orgs are announced on February 22, 2023. Potential GSoC Contributors can apply at [g.co/gsoc](https://g.co/gsoc) from February 22 - March 19, 2023.

Got questions? Email [gsoc-support@google.com](mailto:gsoc-support@google.com).


### About The Society for Arts and Technology [SAT]

The Society for Arts and Technology [SAT] is a non-profit organization recognized internationally for the development and creative use of immersive technologies, virtual reality and high-speed networks. Founded in 1996, its triple mission as a center for the arts, training and research, is to enable and host teleimmersive multisensorial experiences. 

Open-source tools created and supported at SAT include:

- **Immersive rendering**: 
[Mirador](https://gitlab.com/sat-mtl/tools/mirador) (Facilitate the deployment of temporary immersive spaces)
- **Interaction**: 
[LivePose](https://gitlab.com/sat-mtl/tools/livepose/) (Tool for capturing human poses and movements in real time using computer vision)
- **Projection mapping**: 
[Splash](https://gitlab.com/sat-mtl/tools/splash/splash) (Real time projection mapping engine), 
[Calimiro](https://gitlab.com/sat-mtl/tools/splash/calimiro/) (Tool for the calibration of immersive environment by structured light)
- **Telepresence**: 
[Satellite hub](https://hub.satellite.sat.qc.ca) (Immersive 3D social web environment), 
[Scenic](https://gitlab.com/sat-mtl/tools/scenic/scenic/) (Interacting with Switcher through a graphical user interface), 
[Switcher](https://gitlab.com/sat-mtl/tools/switcher) (Tool for creating collaborative experiences over the Internet)
- **Audio spatialization**: 
[Audiodice](https://gitlab.com/sat-mtl/tools/audiodice) (Populate the physical space with sound sources), 
[SATIE](https://gitlab.com/sat-mtl/tools/satie/satie) (Audio spatialization engine for dense live audio scenes), 
[vaRays](https://gitlab.com/sat-mtl/tools/vaRays) (Real time calculation of impulse responses for a virtual environment)
- **Glue and inter-operability**: 
[shmdata](https://gitlab.com/sat-mtl/tools/shmdata) (Zero-copy transmission between software on the same computer),
[libmapper](https://github.com/libmapper/libmapper/) (a library for mapping signals), 
[Puara](https://github.com/puara) (framework for building and deploy new media installations and New Interfaces for Music Expression (NIME))
- **Authoring of multisensorial multimedia immersions**: 
[blender](https://www.blender.org/), 
[OSSIA score](https://ossia.io/), 
[webmapper](https://github.com/libmapper/webmapper/)


### Submit proposals to apply as student contributors at SAT

We encourage participation from students from communities under-represented in the Science, Technology, Engineering, Arts, and Mathematics (STEAM) fields. We aim at increasing the participation of groups traditionally underrepresented in software development (including but not limited to: women, LGBTQ+, underrepresented minorities, and people with disabilities).

To submit proposals to apply as student contributors at SAT, please:

* confirm that you are eligible to participate as student contributor by reading section `7. GSoC Contributors` of GSoC rules: [https://summerofcode.withgoogle.com/rules](https://summerofcode.withgoogle.com/rules)
* confirm that you are available during the GSoC timeline: [https://developers.google.com/open-source/gsoc/timeline](https://developers.google.com/open-source/gsoc/timeline)
* read the GSOC contributor guide: [https://google.github.io/gsocguides/student/](https://google.github.io/gsocguides/student/)
* browse our project ideas at SAT: [https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/categories/ideas/](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/categories/ideas/)
* read our proposals guide at SAT: [https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2023-proposals/proposal-welcome/](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2023-proposals/proposal-welcome/)
* sign [up](https://gitlab.com/users/sign_up)/[in](https://gitlab.com/users/sign_in) on [gitlab.com](https://gitlab.com)
* create an issue on: [https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/issues](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/issues)

We will then work together with you contributors so that you can submit your proposal, first as a [merge request to our repository](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/merge_requests) and then by applying on [https://summerofcode.withgoogle.com](https://summerofcode.withgoogle.com) as required by the GSoC program.

GSoC 2023 at SAT: [https://summerofcode.withgoogle.com/programs/2023/organizations/society-for-arts-and-technology-sat](https://summerofcode.withgoogle.com/programs/2023/organizations/society-for-arts-and-technology-sat)
