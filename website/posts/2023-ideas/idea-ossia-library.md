<!--
.. title: Interfaces for media library exploration in ossia score
.. slug: idea-ossia-media-library
.. author: Jean-Michaël Celerier
.. date: 2023-03-15 08:20:00 UTC+01:00
.. tags: ideas, medium, 350 hours
.. type: text
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

Interfaces for media library exploration in ossia score

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

This project is about helping users of a multimedia composition software to easily browse their media library, which may contain thousands of files. 
Take for example sound collections: the tool would organize sounds visually in a map according to their sonic similarity: the long, dark bass sounds would be close together, just like the short high-pitched trumpet sounds. 
Multiple environments of this sort already exist (like [AudioStellar](https://audiostellar.xyz/), [FluCoMa](https://github.com/flucoma), [MediaCycle](https://github.com/MediaCycle)): the idea is to review them and either implement communication mechanisms between them and the software ossia score, or to develop similar ideas in C++ & Qt directly in ossia.

## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

- A software library which can be used by novice composers, with interactions such as hovering, drag'and'drop, etc.
- Code that works under Linux -- cross-platform is a plus. 
- Happy multimedia artists :-)

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 

- C++
- Potentially machine learning skills

## Possible mentors

<!-- Please list each mentor hyperlinked to their gitlab profile. 2 possible mentors are more failsafe. -->

[Christian Frisson](https://gitlab.com/ChristianFrisson)
[Jean-Michaël Celerier](https://gitlab.com/jcelerier)

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

medium
