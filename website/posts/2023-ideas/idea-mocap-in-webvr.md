<!--
.. title: Motion Capture in WebXR
.. slug: idea-mocap-in-webvr
.. author: Manuel Bolduc
.. date: 2023-03-10 14:24:11 UTC-05:00
.. tags: ideas, medium, 350 hours
.. type: text
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

Represent Pose detection using Mediapipe in a WebXR environment.

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

[Mediapipe Pose](https://github.com/google/mediapipe/blob/master/docs/solutions/pose.md) is a free and open source software for pose detection in live streaming context. 

The SAT is exploring opportunities for replaying concerts in Extended Reality (XR) by developing a platform prototype where XR users in telepresence settings can relive a live performance through motion capture data replay. 
The GSoC contributor will build a prototype implemented in [WebXR](https://immersiveweb.dev/) to replay motion capture from live performances.

A similar GSoC contribution was achieved last year with the SAT, [Face tracking for improving accessibility in hybrid telepresence interactions](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2022-ideas/idea-face-tracking-livepose-satellite/). While Matt Wiese's [efforts](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2022-contributions/matthewwiese-satellite-face-tracking/work-product-matthewwiese/) applied pose detection algorithms to help XR users navigate virtual worlds, in this project idea we seek to apply pose detection algorithms to the virtual world itself: to simulate a live performance context. 


## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

* Create a XR session for presenting real-time performances
* Gain experience with technology integration

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 

* required: some experience with coding
* preferred: some experience with Free and Open Source Software (FOSS)
* preferred: some experience with Python
* preferred: some interest in computer graphics

## Possible mentors

<!-- Please list each mentor hyperlinked to their gitlab profile. 2 possible mentors are more failsafe. -->

[Manuel Bolduc](https://gitlab.com/bolducmanuel)
[Christian Frisson](https://gitlab.com/ChristianFrisson)
[Rochana Fardon](https://gitlab.com/rfardon)

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 h

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

medium
