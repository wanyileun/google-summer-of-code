<!--
.. title: Embedded sequencer for media arts: ossia in puara
.. slug: idea-ossia-x-puara
.. author: Jean-Michaël Celerier
.. date: 2023-03-15 08:20:00 UTC+01:00
.. tags: ideas, medium, 350 hours
.. type: text
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

Embedded sequencer for media arts: ossia in puara

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

This project is about integrating the media sequencer [Ossia Score](https://ossia.io/score/about.html) inside the [Puara/MPU](https://github.com/Puara/MPU) environment and workflow. 
[Ossia](https://ossia.io/) is a sequencer which allows artists to write scores for media arts which involve audio, video, sensors, microcontrollers, etc. 
[Puara](https://github.com/Puara/) is a framework for building and deploying new media installations, immersive spaces, and New Interfaces for Music Expression (NIME).
Specifically, remote control facilities must be implemented to allow the sequencer to be controlled when it is embedded (e.g., Raspberry Pi or Nvidia Jetson) in an interactive installation that does not have a screen or it is physically out of reach.

## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

- A way to remote-control the Ossia Score sequencer when it is shipped as part of Puara/MPU.
- This could be a graphical web interface: multiple prototypes have already been made.

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 
 
- C++ & Qt
- HTML & JS
- webAssembly is a plus as prototypes have already been made with this technology.

## Possible mentors

<!-- Please list each mentor hyperlinked to their gitlab profile. 2 possible mentors are more failsafe. -->

[Edu Meneses](https://gitlab.com/EduMeneses)
[Jean-Michaël Celerier](https://gitlab.com/jcelerier)

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

medium
